'use strict'

// import 'regenerator-runtime/runtime'

// config
var APP = {
	bp: {
		mobile: 959,
	},
}
var isMobile = function isMobile() {
	return window.matchMedia('(max-width: ' + APP.bp.mobile + 'px)').matches
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError('Cannot call a class as a function')
	}
}

function _defineProperties(target, props) {
	for (var i = 0; i < props.length; i++) {
		var descriptor = props[i]
		descriptor.enumerable = descriptor.enumerable || false
		descriptor.configurable = true
		if ('value' in descriptor) descriptor.writable = true
		Object.defineProperty(target, descriptor.key, descriptor)
	}
}

function _createClass(Constructor, protoProps, staticProps) {
	if (protoProps) _defineProperties(Constructor.prototype, protoProps)
	if (staticProps) _defineProperties(Constructor, staticProps)
	Object.defineProperty(Constructor, 'prototype', { writable: false })
	return Constructor
}

var PjaxEvent = /*#__PURE__*/ (function () {
	function PjaxEvent(el, opt) {
		_classCallCheck(this, PjaxEvent)

		this.loading = '.js-loading'
		this.svg = this.loading + '-svg'
		this.cacheLimit = 0 // 7day 86400 * 7 14400

		this.isCache = this.isCache()
		this.event()
		this.doc = document.documentElement
	}

	_createClass(PjaxEvent, [
		{
			key: 'event',
			value: function event() {
				var _this = this

				barba.init({
					debug: true,
					views: [
						{
							namespace: 'top',
							beforeEnter: function beforeEnter(data) {
								return new Promise(function (resolve, reject) {
									setTimeout(function () {
										resolve()
									}, 500)
								})
							},
							afterEnter: function afterEnter(data) {
								_this.mouseStoker = new MouseStoker(
									'.js-follower'
								)
							},
							beforeLeave: function beforeLeave(data) {
							},
							afterLeave: function afterLeave() {
								_this.mouseStoker.off()
							},
						},
					],
					transitions: [
						{
							beforeOnce: function beforeOnce() {},
							once: function once() {},
							afterOnce: function afterOnce() {},
							beforeLeave: function beforeLeave() {
								_this.leaveEvent()
							},
							leave: function leave(data) {
								return _asyncToGenerator(
									/*#__PURE__*/ regeneratorRuntime.mark(
										function _callee() {
											return regeneratorRuntime.wrap(
												function _callee$(_context) {
													while (1) {
														switch (
															(_context.prev =
																_context.next)
														) {
															case 0:
																if (
																	(typeof parallaxEvent ===
																	'undefined'
																		? 'undefined'
																		: _typeof(
																				parallaxEvent
																		  )) ===
																	'object'
																)
																	parallaxEvent.offScroll()
																_context.next = 3
																return new Promise(
																	function (
																		resolve
																	) {
																		return setTimeout(
																			resolve,
																			1500
																		)
																	}
																)

															case 3:
															case 'end':
																return _context.stop()
														}
													}
												},
												_callee
											)
										}
									)
								)()
							},
							afterLeave: function afterLeave() {},
							beforeEnter: function beforeEnter() {
								var textSplit = new TextSplit('.js-textSplit')
							},
							enter: function enter(data) {
								_this.enterEvent()
							},
							afterEnter: function afterEnter(data) {},
							after: function after() {},
						},
					],
				})
			},
		},
		{
			key: 'leaveEvent',
			value: function leaveEvent() {
				var tl = gsap.timeline()
				tl.to(this.loading, {
					duration: 0.2,
					alpha: 1,
					display: 'block',
				})
					.set(this.svg, {
						attr: {
							d: 'M 0 100 V 100 Q 50 100 100 100 V 100 z',
						},
					})
					.to(this.svg, {
						duration: 0.4,
						ease: 'power2.in',
						attr: {
							d: 'M 0 100 V 50 Q 50 0 100 50 V 100 z',
						},
					})
					.to(this.svg, {
						duration: 0.2,
						ease: 'power2',
						attr: {
							d: 'M 0 100 V 0 Q 50 0 100 0 V 100 z',
						},
					})
			},
		},
		{
			key: 'enterEvent',
			value: function enterEvent() {
				gsap.to(window, {
					scrollTo: {
						y: 0,
						autoKill: false,
						duration: 0.2,
					},
				})
				var tl = gsap.timeline()
				tl.set(this.svg, {
					attr: {
						d: 'M 0 0 V 100 Q 50 100 100 100 V 0 z',
					},
				})
					.to(this.svg, {
						duration: 0.4,
						ease: 'power2.in',
						attr: {
							d: 'M 0 0 V 0 Q 50 100 100 0 V 0 z',
						},
						onComplete: function onComplete() {
							$(window).trigger('enter')
						},
					})
					.to(this.svg, {
						duration: 0.2,
						ease: 'power2',
						attr: {
							d: 'M 0 0 V 0 Q 50 0 100 0 V 0 z',
						},
					})
					.to(this.loading, {
						duration: 0.2,
						alpha: 0,
						display: 'none',
					})
			},
		},
		{
			key: 'getCacheTime',
			value: function getCacheTime() {
				return Math.floor((new Date() * 1) / 1000)
			},
		},
		{
			key: 'isCache',
			value: function isCache() {
				var now = this.getCacheTime()
				var visited = localStorage.getItem(this.cacheName) * 1
				var diffTime = now - visited
				var isVisited = true

				if (!visited || this.cacheLimit < diffTime) {
					// localStorage.setItem(this.cacheName, now)
					isVisited = false
				}

				return isVisited
			},
		},
	])
	return PjaxEvent
})()


var MouseStoker = /*#__PURE__*/ (function () {
	function MouseStoker(el, opt) {
		_classCallCheck(this, MouseStoker)

		this.el = el
		this.followItem = this.el + '-Item'
		this.followItemArea = this.el + '-ItemArea'
		this.mouseX = 0
		this.mouseY = 0
		this.itemTween
		this.scrollTop

		if (!isMobile() && $(this.followItem).length) {
			this.mouseMove()
			this.stokerItem()
		}
	}

	_createClass(MouseStoker, [
		{
			key: 'mouseMove',
			value: function mouseMove() {
				var _this18 = this

				$(window).on('mousemove.stoker', function (e) {
					_this18.mouseX = e.clientX
					_this18.mouseY = e.clientY
				})
			},
		},
		{
			key: 'scroll',
			value: function scroll() {
				var _this19 = this

				$(window).on('scroll.stoker', function () {
					_this19.scrollTop = $(window).scrollTop()
				})
				$(window).trigger('scroll.stoker')
			},
		},
		{
			key: 'stokerItem',
			value: function stokerItem() {
				var _this20 = this

				this.itemTween = new Array()
				$(this.followItemArea).each(function (numb, item) {
					$(item)
						.find(_this20.followItem)
						.each(function (i, el) {
							var posX = 0,
								posY = 0,
								posXM = 0,
								posYM = 0
							$(item).on({
								mouseenter: function mouseenter() {
									if ($(el).data('stoker') == 'normal') {
										_this20.itemTween[i] = gsap.to(el, {
											duration: 0.01,
											repeat: -1,
											onRepeat: function onRepeat() {
												posX +=
													_this20.mouseX -
													posX -
													$(el).offset().left
												posY +=
													_this20.mouseY -
													posY -
													$(el).offset().top

												if (posY < -700) {
													posY = -700
												}

												if (posX < -700) {
													posX = -700
												}

												gsap.to(el, {
													x: posX * 0.05,
													y: posY * 0.05,
													duration: 0.3,
												})
											},
										})
									} else if (
										$(el).data('stoker') == 'reverse'
									) {
										_this20.itemTween[i] = gsap.to(el, {
											duration: 0.01,
											repeat: -1,
											onRepeat: function onRepeat() {
												posXM +=
													_this20.mouseX -
													posXM -
													$(el).offset().left
												posYM +=
													_this20.mouseY -
													posYM -
													$(el).offset().top

												if (posYM < -700) {
													posYM = -700
												}

												if (posXM < -700) {
													posXM = -700
												}

												gsap.to(el, {
													x: posXM * -0.05,
													y: posYM * -0.05,
													duration: 0.3,
												})
											},
										})
									}
								},
								mouseleave: function mouseleave() {
									_this20.itemTween[i] = gsap.to(el, {
										x: 0,
										y: 0,
										duration: 0.3,
									})

									_this20.itemTween[i].kill()
								},
							})
						})
				})
			},
		},
		{
			key: 'off',
			value: function off() {
				$(window).off('mousemove.stoker') // $(window).off('scroll.stoker');
			},
		},
	])

	return MouseStoker
})()

// $(function () {
	setTimeout(()=>{
    var pjaxEvent = new PjaxEvent()
  },1000)

// })

// export const initRender = () => {
// 	// window.parallaxEvent = new ParallaxEvent('.js-parallax')
// }
// ;

