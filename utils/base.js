export function fnConvertTextCharacter (val, classText, classMain) {
  let resNeed = ''
  let oneText = []
  if (val.includes('<br>')) {
    val.split('<br>').forEach((item) => {
      item.split('').forEach((itemC) => {
        if (itemC === ' ') {
          oneText.push(`</div><div style="display: inline"><span style="opacity: 0" class="${classMain}  ${classText} line-height-150">&nbsp;</span>`)
        } else {
          oneText.push(`<span style="display: inline" class="${classMain}  ${classText} line-height-150">${itemC}</span>`)
        }
      })
      resNeed = resNeed + oneText.join('') + '<br>'
      oneText = []
    })
  } else {
    val.split('').forEach((itemC) => {
      if (itemC === ' ') {
        oneText.push(`</div><div style="display: inline"><span style="opacity: 0" class="${classMain}  ${classText} line-height-150">&nbsp;</span>`)
      } else {
        oneText.push(`<span style="display: inline" class="${classMain}  ${classText}  line-height-150">${itemC}</span>`)
      }
    })
    resNeed = resNeed + oneText.join('')
    oneText = []
  }

  return '<div style="display: inline">' + resNeed + '</div>'
}

export function fnMountedTextCharacter (classMain) {
  const arr1 = document.querySelectorAll(classMain)
  for (let i = 0; i < arr1.length; i++) {
    arr1[i].style.transitionDelay = `${i * 0.04}s`
    arr1[i].classList.add('is-fadein')
  }
}

export function fnMountedCard (classMain, timings) {
  const arr1 = document.querySelectorAll(classMain)
  for (let i = 0; i < arr1.length; i++) {
    arr1[i].style.transitionDelay = `${i * timings}s`
    arr1[i].classList.add('is-fadein')
  }
}

export async function copyValue(text) {
  console.log('text', text)
  if (window?.isSecureContext && navigator?.clipboard) {
    await navigator?.clipboard?.writeText(text);
  }
}
