export default {
  server: {
    port: process.env.LISTEN_PORT || 8000,
    host: '0.0.0.0'
  },
  head: {
    title: process.env.TITLE || 'Omega Network - The Future Of Cryptocurrency',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'robot', content: 'max-image-preview:large'},
      { property: 'title', content: process.env.TITLE},
      { hid: 'description', name: 'description', content: 'OM makes the crypto mining easier by using a breakthrough tech that allows you to manage it on your phone.' },
      { property: 'image', content: `${process.env.USER_URL}img-description.png`},
      { property: 'og:title', content: process.env.TITLE},
      { hid: 'og:description', name: 'og:description', content: 'OM makes the crypto mining easier by using a breakthrough tech that allows you to manage it on your phone.' },
      { hid: 'og:image', property: 'og:image', content: `${process.env.USER_URL}img-description.png`},
      { property: ' twitter:title', content: process.env.TITLE},
      { hid: 'twitter:description', name: 'twitter:description', content: 'OM makes the crypto mining easier by using a breakthrough tech that allows you to manage it on your phone.' },
      { hid: 'twitter:image', property: 'og:image', content: `${process.env.USER_URL}img-description.png`},
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/logo.png' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap'
      }
    ],
    script: [
      {
        type: 'text/javascript',
        src: '/js/jquery.min.js',
        body: true
      },
      {
        type: 'text/javascript',
        src: '/js/barba.min.js',
        body: true
      },
      {
        type: 'text/javascript',
        src: '/js/gsap.min.js',
        body: true
      },
      {
        type: 'text/javascript',
        src: '/js/scripts.js',
        body: true,
        crossorigin: 'anonymous'
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/slick.css',
    '@/assets/css/slick-themes.css',
    '@/assets/css/main.css',
    '@/assets/css/animation.scss',
    '@/assets/css/animation_pro.scss',
    '@/assets/css/index.scss',
    'element-plus/dist/index.css'

  ],

  plugins: [
    { src: '@/plugins/element-ui' },
    { src: '@/plugins/vue-slick-carousel' },
    { src: '@/plugins/device', mode: 'client' }
  ],

  components: true,

  buildModules: [
    '@nuxtjs/moment',
    '@nuxt/postcss8'
  ],

  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    "nuxt-clipboard2"
  ],

  axios: {
    baseURL: '/'
  },

  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  content: {},

  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {}
      }
    }
  }
}
