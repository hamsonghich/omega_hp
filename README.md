### npm

6. Run `npm install` to install dependencies
7. Run `npm run dev` to start the server

### yarn

6. Run `yarn install` to install dependencies
7. Run `yarn run dev` to start the server

## Installation (with license)

### npm

9. Run `npm install` to install dependencies
10. Run `npm run dev` to start the server

### yarn

9. Run `yarn install` to install dependencies
10. Run `yarn run dev` to start the server

